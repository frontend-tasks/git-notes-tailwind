import { GITHUB_OAUTH_URL } from "../constants";

export async function getAccessToken(setAccessToken, codeParams) {
  await fetch(`http://localhost:4000/getAccessToken?code=${codeParams}`, {
    method: "GET",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      if (data.access_token) {
        setAccessToken(data.access_token);
      }
    });
}

export async function fetchUserData(setUserData, accessToken) {
  await fetch("http://localhost:4000/getUserData", {
    method: "GET",
    headers: {
      Authorization: "Bearer " + accessToken,
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      setUserData(data);
    });
}

export function loginWithGithub() {
  try {
    // Redirect the user to the GitHub OAuth authorization URL
    window.location.href = `${GITHUB_OAUTH_URL}?client_id=${process.env.REACT_APP_CLIENT_ID}`;
  } catch (error) {
    console.error("GitHub login error:", error);
  }
}
